# vuelidate-messages

A small library to help build error messages from Vuelidate's validation state.

## Installation

```sh
npm install vuelidate-messages --save
```

## Usage

```html
<template>
  <div>
    <input v-model="name" />
    <span>{{validationMsg($v.name)}}</span>
  </div>
</template>

<script>
import { required, minLength } from 'vuelidate/lib/validators'
import { validationMessage } from 'vuelidate-messages'

const formMessages = {
  required: () => 'Required',
  txtMinLen: ({ $params }) => `Must be at least ${$params.txtMinLen.min} characters.`
}

export default {
  data () {
    return { name: '' }
  },
  validations: {
    name: {
      required,
      txtMinLen: minLength(2)
    }
  },
  methods: {
    validationMsg: validationMessage(formMessages)
  }
}
</script>

```

## API

### validationMessages(messages [, options])

Returns a function that accepts a Vuelidate field.  The return value of
that function is an array of validation messages.

```
import { validationMessages } from 'vuelidate-messages'

// options
{
  // Require the field to be dirty before returning an error. (default = true)
  dirty: Boolean,

  // Limit the number of error messages that can be returned. (default = 1)
  first: Number
}
```

### validationMessage(messages [, options])

Returns a function that accepts a Vuelidate field.  The return value of
that function is one validation message (string). An empty string is returned if the field is valid.

```
import { validationMessage } from 'vuelidate-messages'

// options
{
  // Require the field to be dirty before returning an error. (default = true)
  dirty: Boolean
}
```

## Message context (this)

Your messsage functions are called with the context of the Vue component, so you have access to any
plugins, state, etc within the message function.  In order for this to work, you cannot use arrow functions.
The context of an arrow function cannot be specified at invocation.
Here is an example of a message function that uses [vue-i18n](https://kazupon.github.io/vue-i18n/).

```
const i18nMessages = {
  en: {
    validation: {
      txtMinLen: 'Must have {n} character | Must have {n} characters'
    }
  }
}

const formMessages = {
  txtMinLen: function ({ $params }) {
    return this.$tc('validation.txtMinLen', $params.txtMinLen.min)
  }
}
```
