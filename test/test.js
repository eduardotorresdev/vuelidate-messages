'use strict'
const assert = require('assert')
const { validationMessage, validationMessages } = require('../dist/index.js')

const messages = {
  required: (_, name) => `${name} is required`,
  minLength: ({ $params }) => `Must be at least ${$params.minLength.min} characters.`,
  between: function ({ $params }) {
    return `${this.name} must be between ${$params.between.min} and ${$params.between.max} characters.`
  }
}

describe('Vuelidate Messages', () => {
  const getValidationMessage = validationMessage(messages)
  const getValidationMessages = validationMessages(messages, { first: 3 })

    it('should return return the first validation message for which the field\'s value is invalid', () => {
      const field = { $dirty: true, required: false, minLength: false }
      const msg = getValidationMessage(field, 'MyField')
      assert.strictEqual(msg, 'MyField is required')
    })

    it('should return return all validation messages as an array', () => {
      const field = { $dirty: true, required: false, minLength: false, $params: { minLength: { min: 4 } } }
      const msg = getValidationMessages(field, 'MyField')
      assert.deepEqual(msg, ['MyField is required', `Must be at least 4 characters.`])
    })

    it('should return a message with the params of the field', () => {
      const field = { $dirty: true, minLength: false, $params: { minLength: { min: 4 } } }
      const msg = getValidationMessage(field)
      assert.strictEqual(msg, `Must be at least 4 characters.`)
    })

    it('should receive the context of the curried default function', () => {
      const context = { name: 'Bing Bong' }
      const field = { $dirty: true, between: false, $params: { between: { min: 20, max: 30 } } }
      const msg = getValidationMessage.call(context, field)
      assert.strictEqual(msg, `Bing Bong must be between 20 and 30 characters.`)
    })

    it('should return "Invalid" when no message creator is provided', () => {
      const field = { $dirty: true, email: false }
      const msg = getValidationMessage(field)
      assert.strictEqual(msg, 'Invalid')
    })
});
